package com.nespresso.exercise.electric_trip;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ElectricTrip {

	protected Map<Ville, DestBean> map = new HashMap<Ville, DestBean>();
	protected Map<Integer, Participant> participants = new HashMap<Integer, Participant>();

	private static final DecimalFormat myFormatter = new DecimalFormat("#%");
	private static Integer CURRENT_ID = 1;

	public ElectricTrip(String mapString) {
		if (mapString != null) {
			String[] tabMap = mapString.split("-");
			Ville villeDepart = null;
			Ville villeDestination = null;
			String[] detail = null;
			for (int i = 0; i < tabMap.length - 1;) {
				String code = tabMap[i] ;
				if(code.contains(":")){
					detail = code.split(":");
					villeDepart = new Ville(detail[0],Tools.parseInt( detail[1]));
				}else{
					villeDepart = new Ville(code);
				}
				code = tabMap[i+2] ;
				if(code.contains(":")){
					detail = code.split(":");
					villeDestination = new Ville(detail[0],Tools.parseInt( detail[1]));
				}else{
					villeDestination = new Ville(code);
				}

				map.put(villeDepart, new DestBean(tabMap[i + 1], villeDestination));
				i += 2;
			}
		}
	}

	public int startTripIn(String villeDepart, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		participants.put(CURRENT_ID, new Participant(CURRENT_ID, new Ville(villeDepart),
				batterySize, lowSpeedPerformance, highSpeedPerformance));
		return CURRENT_ID++;
	}

	public void go(int participantId) {
		travel(participantId,true);
	}

	private void travel(int participantId, boolean mode) {

		Participant participant = participants.get(participantId);
		int speedPerformance = mode? participant.lowSpeedPerformance:participant.highSpeedPerformance;
		DestBean destinationBean = null;
		double distanceDispo = 0;
		int nextCharger = 0 ;
		boolean test = false;
		do{
			destinationBean = map.get(participant.location);
			distanceDispo = (participant.currentCharge * participant.batterySize)
					* speedPerformance;
			nextCharger = getDistToNextCharger(participant.location);
			
			test = destinationBean!=null && (destinationBean.distance < distanceDispo &&( distanceDispo>nextCharger ) ||nextCharger>getDistToEndCity(participant.location) );
			if (test) {
				participant.location = destinationBean.villeDestination;
				participant.currentCharge = ((participant.currentCharge * participant.batterySize) - (destinationBean.distance / speedPerformance))
						/ participant.batterySize;
			}
		}while(test);

	}

	public String locationOf(int participantId) {
		String location = "";
		Participant participant = participants.get(participantId);
		if (participant != null && participant.location != null) {
			location = participant.location.code;
		}
		return location;
	}

	public String chargeOf(int participantId) {
		String charge = "";
		Participant participant = participants.get(participantId);
		if (participant != null) {
			charge = myFormatter.format(participant.currentCharge);
		}
		return charge;
	}

	public void sprint(int participantId) {
		travel(participantId,false);
	}

	public void charge(int participantId, int hoursOfCharge) {
		Participant participant = participants.get(participantId);
		double currentCharge = Math.min(participant.currentCharge + ( participant.location.chargerPawer * hoursOfCharge  / Double.valueOf(participant.batterySize) ) ,1);
		participant.currentCharge = Math.floor( currentCharge * 100 ) / 100D ;

	}


	protected int getDistToNextCharger(Ville villeSrc){
		int dist = 0 ;
		boolean existCharger= false;
		if(villeSrc!=null){
			DestBean nextDestination = null;
			int chargerPawer = -1;
			do{
				nextDestination = map.get(villeSrc);
				if(nextDestination!=null){
					dist += nextDestination.distance ;
					chargerPawer = nextDestination.villeDestination.chargerPawer;
					existCharger = nextDestination.villeDestination.chargerPawer!=0 ||  villeSrc.chargerPawer!=0;
					villeSrc = nextDestination.villeDestination;
				}
			}while(nextDestination != null && chargerPawer == 0);

		}
		return existCharger ? dist:0;
	}
	protected int getDistToEndCity(Ville villeSrc){
		int dist = 0 ;
		if(villeSrc!=null){
			DestBean nextDestination = null;
			do{
				nextDestination = map.get(villeSrc);
				if(nextDestination!=null){
					dist += nextDestination.distance ;
					villeSrc = nextDestination.villeDestination;
				}
			}while(nextDestination != null);

		}
		return dist;
	}

}

class DestBean implements Serializable {

	private static final long serialVersionUID = 50681709152731359L;
	protected int distance;
	protected Ville villeDestination;

	public DestBean(String distanceS, Ville destination) {
		this.distance = Tools.parseInt(distanceS);
		villeDestination = destination ;
	}

}

class Participant {

	protected Integer id;
	protected Ville location;
	protected int batterySize; // KWh
	protected int lowSpeedPerformance; // Km per KWh
	protected int highSpeedPerformance; // Km per KWh
	protected double currentCharge;

	public Participant(Integer id, Ville villeDepart, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		super();
		this.location = villeDepart;
		this.id = id;
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
		this.currentCharge = 1;
	}
}

class Ville {
	protected String code;
	protected int chargerPawer;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ville other = (Ville) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	public Ville(String code, int chargerPawer) {
		super();
		this.code = code;
		this.chargerPawer = chargerPawer;
	}

	public Ville(String code) {
		super();
		this.code = code;
		this.chargerPawer = 0;
	}
}

class Tools{
	public static Integer parseInt (String s){
		Integer result = null;
		try {
			result = Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			System.err.println("ERROR_TO_PARSE_DISTANCE");
		}
		return result ;
	}
}